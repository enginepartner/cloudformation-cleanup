'use strict';

module.exports.main = (event, context, callback) => {
  var
    ecr,
    response = require('./cfn-response');

  function main() {
    initEcr();

    repository() ? listImages() : completeCallback();
  }

  function initEcr() {
    var ECR = require('aws-sdk/clients/ecr');
    ecr = new ECR();
  }

  function completeCallback() {
    response.send(event, context, response.SUCCESS, { }, physicalResourceId());
    callback();
  }

  function errorCallback(error) {
    console.log(error, error.stack);
    response.send(event, context, response.FAILED, error, physicalResourceId());
    callback(error);
  }

  function repository() {
    if (!(event instanceof Object))
      return false;

    if (!('RequestType' in event) || event.RequestType != 'Delete')
      return false;

    if (!('ResourceProperties' in event) || !('Ecr' in event.ResourceProperties))
      return false;

    return event.ResourceProperties.Ecr;
  }

  function listImages() {
    ecr.listImages(listParams(), listCallback);
  }

  function listParams() {
    return { repositoryName: repository() };
  }

  function listCallback(error, data) {
    if (error)
      return errorCallback(error);

    deleteDigests(data);

    hasNextPage(data) ? listImages() : completeCallback();
  }

  function hasNextPage(data) {
    return 'nextToken' in data && data.nextToken;
  }

  function deleteDigests(data) {
    var digests = extractDigests(data.imageIds);

    if (!digests.length)
      return;

    ecr.batchDeleteImage(deleteParams(digests), deleteCallback);
  }

  function extractDigests(images) {
    var digests = images.map(image => image.imageDigest);
    var digestSet = { }

    digests.forEach(digest => { digestSet[digest] = null; });
    return Object.keys(digestSet);
  }

  function deleteParams(digests) {
    return {
      repositoryName: repository(),
      imageIds: digests.map(
        digest => { return { imageDigest: digest }; }
      )
    };
  }

  function deleteCallback(error, data) {
    error ? errorCallback(error) : console.log(data);
  }

  function physicalResourceId() {
    return 'clean-up-ecr';
  }

  main();
};
