'use strict';

module.exports.main = (event, context, callback) => {
  var
    s3,
    response = require('./cfn-response');

  function main() {
    init();
    bucket() ? listObjects() : completeCallback();
  }

  function init() {
    var S3 = require('aws-sdk/clients/s3');
    s3 = new S3();
  }

  function listObjects() {
    s3.listObjectsV2(listParams(), listCallback);
  }

  function listParams() {
    return { Bucket: bucket() };
  }

  function listCallback(error, data) {
    if (error)
      return errorCallback(error);

    if (!data.KeyCount)
      return completeCallback();

    deleteObjects(data.Contents, deleteCallback);
  }

  function bucket() {
    if (!(event instanceof Object))
      return false;

    if (!('RequestType' in event) || event.RequestType != 'Delete')
      return false;

    if (!('ResourceProperties' in event) || !('Bucket' in event.ResourceProperties))
      return false;

    return event.ResourceProperties.Bucket;
  }

  function completeCallback() {
    response.send(event, context, response.SUCCESS, { }, physicalResourceId());
    callback();
  }

  function errorCallback(error) {
    console.log(error, error.stack);
    response.send(event, context, response.FAILED, error, physicalResourceId());
    callback(error);
  }

  function deleteObjects(contents) {
    s3.deleteObjects(deleteParams(contents), deleteCallback);
  }

  function deleteCallback(error, data) {
    error ? errorCallback(error) : console.log(data);

    listObjects();
  }

  function deleteParams(contents) {
    return {
      Bucket: bucket(),
      Delete: {
        Objects: keys(contents)
      }
    };
  }

  function keys(contents) {
    return contents.map(x => { return { Key: x.Key }; });
  }

  function physicalResourceId() {
    return 'clean-up-s3';
  }

  main();
};
