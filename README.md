# CloudFormation Cleanup

[![Build Status](https://semaphoreci.com/api/v1/engine-partner/cloudformation-cleanup/branches/master/badge.svg)](https://semaphoreci.com/engine-partner/cloudformation-cleanup)

Some of the CloudFormation resources cannot be deleted when they are not empty,
for example, an S3 bucket with objects and an ECR repository with images.

This project contains the functions for cleaning up these resources.

## Development and tesing

Copy `env.sh.example` to `env.sh` and editing it according to your need.

Please use these scripts to carry out the corresponding tasks:

1. `bin/prepare`
1. `bin/test`
1. `bin/deploy`
1. `bin/remove`

## Usage

To use the functions, you can reference the ARNs from custom resources, like
so:

```
AWSTemplateFormatVersion: '2010-09-09'
Parameters:
  CleanupStack:
    Type: String
Resources:
  Bucket:
    Type: AWS::S3::Bucket
  BucketCleanUpOnDelete:
    Properties:
      ServiceToken:
        Fn::ImportValue:
          !Sub "${CleanupStack}-clean-up-s3-bucket"
      Bucket: !Ref Bucket
    Type: Custom::CleanUpS3
```
